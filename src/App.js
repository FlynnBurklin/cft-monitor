import './App.css'
import CFTList from './Components/CFTList'
import {APIResultsProvider} from './Contexts/APIResultsContext'

function App() {
  return (
    <APIResultsProvider>
      <div className="App">
        <header className="App-header">
          <CFTList/>
        </header>
      </div>
    </APIResultsProvider>
  )
}

export default App

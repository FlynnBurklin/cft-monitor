import {createContext, useState} from 'react'

export const APIResultsContext = createContext({})

export const APIResultsProvider = ({children}) => {
  const [apiResults, setApiResults] = useState({})

  // Are these huge timesavers? No. Do they prevent strange unintentional data corruption? Sure hope so!
  const getNamedResult = (name) => {
    return apiResults[name]
  }

  const setNamedResult = (name, result) => {
    setApiResults({...apiResults, [name]: result})
  }

  return (
    <APIResultsContext.Provider value={{getNamedResult, setNamedResult}}>
      {children}
    </APIResultsContext.Provider>
  )
}
import dayjs from 'dayjs'
const localizedFormat = require('dayjs/plugin/localizedFormat')
dayjs.extend(localizedFormat)

const CFTResult = ({testResult}) => {
    console.log(testResult)
    return (
        <div className='cft-result-row'>{testResult.suiteName} ran in {testResult.env} at {dayjs(testResult.lastRan).format('LLL')} with status {testResult.result}. Has run a total of {testResult.count} times.</div>
    )
}

export default CFTResult
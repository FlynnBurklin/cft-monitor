import dayjs from 'dayjs'
import { TabList, Tab, TabPanel, Tabs } from 'react-tabs'
import 'react-tabs/style/react-tabs.css'
import BranchBasedSuites from './BranchBasedSuites'
import ProjectBasedSuites from './ProjectBasedSuites'

const localizedFormat = require('dayjs/plugin/localizedFormat')
dayjs.extend(localizedFormat)

const CFTList = () => {
  return (
    <div>
      <Tabs>
        <TabList>
          <Tab>Salesforce Robot Tests</Tab>
          <Tab>BLP Robot Tests</Tab>
        </TabList>
        <TabPanel>
          <BranchBasedSuites />
        </TabPanel>
        <TabPanel>
          <ProjectBasedSuites />
        </TabPanel>
      </Tabs>
    </div>
  )
}

export default CFTList

export const Columns = [
  {
    name: 'Suite Name',
    selector: r => r.suiteName,
    sortable: true,
    width: '200px'
  },
  {
    name: 'Environment',
    selector: r => r.env,
    sortable: true,
    width: '125px'
  },
  {
    name: 'Latest Run',
    selector: r => dayjs(r.lastRan).format('LLL'),
    sortable: true,
    width: '200px'
  },
  {
    name: 'Result',
    selector: r => r.result,
    sortable: true,
    width: '100px'
  },
  {
    name: 'Total Runs',
    selector: r => r.count,
    sortable: true,
    width: '125px'
  }
]
import {memo, useContext, useEffect, useState} from 'react'
import {GetSuitesFromJobs} from '../API/GitlabPipelineAPI'
import DataTable from 'react-data-table-component'
import {Columns} from './CFTList'
import {APIResultsContext} from '../Contexts/APIResultsContext'
import {GET_SUITES_FROM_JOBS_NAME} from '../Utility/Constants'

// TODO: Make this take a project id, so it can represent multiple projects.
const ProjectBasedSuites = () => {
  const {getNamedResult, setNamedResult} = useContext(APIResultsContext)
  const [error, setError] = useState(null)

  useEffect(() => {
    let mounted = true
    if (!getNamedResult(GET_SUITES_FROM_JOBS_NAME)) {
      GetSuitesFromJobs().then(data => {
        if (mounted) {
          setNamedResult(GET_SUITES_FROM_JOBS_NAME, data)
        }
      }).catch(e => {
        setError(e.toString())
      })
    }
    return () => {
      mounted = false
    }
  }, [getNamedResult, setNamedResult])

  let blpContent = null
  if (error) {
    blpContent = <div className="error">{error}</div>
  } else if (!getNamedResult(GET_SUITES_FROM_JOBS_NAME)) {
    blpContent = <div className="cft-result-row">Loading...</div>
  } else {
    blpContent = (
      <>
        <div className="sub-heading">(data from the last 30 days)</div>
        <DataTable columns={Columns} data={getNamedResult(GET_SUITES_FROM_JOBS_NAME)} highlightOnHover/>
      </>
    )
  }

  return (
    <div className="result-container">
      {blpContent}
    </div>
  )
}

export default memo(ProjectBasedSuites)
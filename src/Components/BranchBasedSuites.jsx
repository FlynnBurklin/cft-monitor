import {memo, useContext, useEffect, useState} from 'react'
import {GetSuitesFromPipelines} from '../API/GitlabPipelineAPI'
import DataTable from 'react-data-table-component'
import {Columns} from './CFTList'
import {APIResultsContext} from '../Contexts/APIResultsContext'
import {GET_SUITES_FROM_PIPELINE_NAME} from '../Utility/Constants'

// Is this a bit overly defensive? Sure, it's unlikely that there's going to be another project with a bunch of suites all rolled into it.
// But, still, this allows for that scenario and doesn't needlessly tie this to the current (2021-07-07) purpose of this repository,
// which is Salesforce tests.
const BranchBasedSuites = () => {
  const {getNamedResult, setNamedResult} = useContext(APIResultsContext)
  const [error, setError] = useState(null)

  useEffect(() => {
    let isMounted = true
    if (!getNamedResult(GET_SUITES_FROM_PIPELINE_NAME)) {
      GetSuitesFromPipelines().then(data => {
        if (isMounted) {
          setNamedResult(GET_SUITES_FROM_PIPELINE_NAME, data)
        }
      }).catch(e => {
        setError(e.toString())
      })
    }
    return () => {
      isMounted = false
    }
  }, [getNamedResult, setNamedResult])

  let robotProjectContent = null
  if (error) {
    robotProjectContent = <div className="error">{error}</div>
  } else if (!getNamedResult(GET_SUITES_FROM_PIPELINE_NAME)) {
    robotProjectContent = <div className="cft-result-row">Loading...</div>
  } else {
    robotProjectContent = <DataTable columns={Columns} data={getNamedResult(GET_SUITES_FROM_PIPELINE_NAME)} highlightOnHover/>
  }

  return (
    <div className="result-container">
      {robotProjectContent}
    </div>
  )
}

export default memo(BranchBasedSuites)
import fetch from 'node-fetch'
import dayjs from 'dayjs'

export const GetSuitesFromPipelines = async () => {
  if (!process.env.REACT_APP_GITLAB_API_TOKEN) {
    throw new Error('Need gitlab API token!')
  }
  if (!process.env.REACT_APP_BASE_API_URL) {
    throw new Error('Need base API URL!')
  }
  // May be nifty at some point to have something besides hardcoded config...
  if (!process.env.REACT_APP_PIPELINE_SUITES_SRC) {
    throw new Error('Need Project ID of Pipeline Suite source!')
  }

  let totalPages = 2
  let rawPipelineData = []
  for (let i = 1; i <= totalPages; i++) {
    const response = await fetch(
      process.env.REACT_APP_BASE_API_URL + process.env.REACT_APP_PIPELINE_SUITES_SRC + `/pipelines?per_page=100&page=${i}`,
      {
        method: 'GET',
        headers: {
          'PRIVATE-TOKEN': process.env.REACT_APP_GITLAB_API_TOKEN
        }
      }
    )

    if (response.status >= 400) {
      throw new Error(response.body)
    } else {
      rawPipelineData = rawPipelineData.concat(await response.json())
    }
    totalPages = parseInt(response.headers.get('x-total-pages'))
  }

  const histogramResults = rawPipelineData.reduce((reducerResult, currentValue) => {
    if (!Object.keys(reducerResult).includes(currentValue.ref) && currentValue.ref.includes('+')) {
      const firstHalf = currentValue.ref.substr(0, currentValue.ref.indexOf('+'))
      const secondHalf = currentValue.ref.substr(currentValue.ref.indexOf('+') + 1)
      const flip = ['stage', 'devint'].includes(firstHalf.toLowerCase())
      reducerResult[currentValue.ref] = {
        suiteName: flip ? secondHalf : firstHalf,
        env: flip ? firstHalf : secondHalf,
        lastRan: currentValue['updated_at'],
        result: currentValue.status,
        count: 1
      }
    } else if (Object.keys(reducerResult).includes(currentValue.ref)) {
      reducerResult[currentValue.ref].count++
      if (currentValue['updated_at'] > reducerResult[currentValue.ref].lastRan) {
        reducerResult[currentValue.ref].lastRan = currentValue['updated_at']
        reducerResult[currentValue.ref].result = currentValue.status
      }
    }
    return reducerResult
  }, {})

  const transformedResults = []
  for (const res of Object.keys(histogramResults)) {
    transformedResults.push(histogramResults[res])
  }
  return transformedResults
}

export const GetSuitesFromJobs = async () => {
  if (!process.env.REACT_APP_GITLAB_API_TOKEN) {
    throw new Error('Need gitlab API token!')
  }
  if (!process.env.REACT_APP_BASE_API_URL) {
    throw new Error('Need base API URL!')
  }
  // May be nifty at some point to have something besides hardcoded config...
  if (!process.env.REACT_APP_JOB_SUITES_SRC) {
    throw new Error('Need Project ID of Pipeline Suite source!')
  }

  // Get Pipelines that ran robot tests; so, anything on main, devint, and stage?
  let totalPages = 2
  let pipelines = []
  const refs = ['main', 'devint', 'stage']
  for (const ref of refs) {
    for (let i = 1; i <= totalPages; i++) {
      const response = await fetch(
        process.env.REACT_APP_BASE_API_URL + process.env.REACT_APP_JOB_SUITES_SRC + `/pipelines?per_page=100&ref=&page=${i}&ref=${ref}`,
        {
          method: 'GET',
          headers: {
            'PRIVATE-TOKEN': process.env.REACT_APP_GITLAB_API_TOKEN
          }
        }
      )

      if (response.status >= 400) {
        throw new Error(response.body)
      } else {
        pipelines = pipelines.concat(await response.json())
      }
      totalPages = parseInt(response.headers.get('x-total-pages'))
    }
  }

  const aggregateJobs = []
  const threshold = dayjs(new Date()).add(-1, 'month')
  // Loop through pipelines and get the jobs that are failed or success scope
  for (const pipeline of pipelines) {
    // We are making the assumption we run this at least once a month; that's the goal, at least!
    if (dayjs(pipeline['created_at']) > threshold) {
      const response = await fetch(
        process.env.REACT_APP_BASE_API_URL + process.env.REACT_APP_JOB_SUITES_SRC + `/pipelines/${pipeline.id}/jobs?scope[]=success&scope[]=failed`,
        {
          method: 'GET',
          headers: {
            'PRIVATE-TOKEN': process.env.REACT_APP_GITLAB_API_TOKEN
          }
        }
      )

      if (response.status >= 400) {
        throw new Error(response.body)
      } else {
        const jobs = await response.json()

        for (const job of jobs) {
          if (job.name.toLowerCase().includes('robot ')) {
            aggregateJobs.push(job)
          }
        }
      }
    }
  }

  const histogramResults = aggregateJobs.reduce((reducerResult, currentValue) => {
    if(!Object.keys(reducerResult).includes(currentValue.name)) {
      reducerResult[currentValue.name] = {
        suiteName: 'blp web app',
        env: currentValue.name.split(' ').pop(),
        lastRan: currentValue['finished_at'],
        result: currentValue.status,
        count: 1
      }
    } else {
      reducerResult[currentValue.name].count++
      if (reducerResult[currentValue.name].lastRan < currentValue['finished_at']) {
        reducerResult[currentValue.name].lastRan = currentValue['finished_at']
      }
    }
    return reducerResult
  }, {})

  const transformedResults = []
  for (const res of Object.keys(histogramResults)) {
    transformedResults.push(histogramResults[res])
  }

  return transformedResults
}